#include "windows.h"
#include <list>
#include "../Include/Driver.h"
#include "structs.h"
#include <d3d9.h>
#include "offsets.h"

namespace SDK {
	template <typename Type>
	inline Type read(unsigned long long Address)
	{
		Type Buffer;
		ZeroMemory(&Buffer, sizeof(Type));
		if (!NT_SUCCESS(Driver->ReadProcessMem(Address, &Buffer, sizeof(Type))))
			ZeroMemory(&Buffer, sizeof(Type));
		return Buffer;
	}

	template <typename Type>
	inline void write(UINT_PTR write_address, const Type& value)
	{
		Driver->WriteProcessMem(write_address, &value, sizeof(Type));
	}


	struct Camera
	{
		Vector3 Location;
		Vector3 Rotation;
		float FieldOfView;
	};

	Camera GetCamera(uintptr_t LocalPlayer, uintptr_t Player_Root, uintptr_t World, uintptr_t PlayerController, uintptr_t LocalPawn)
	{
		__int64 v1;
		v1 = read<__int64>(LocalPlayer + 0xd0);
		__int64 v2 = read<__int64>(v1 + 8);

		__int64 ViewportClient = read<__int64>(LocalPlayer + 0x78);
		__int64 AudioHandle = read<__int64>(ViewportClient + 0x98);
		__int64 ViewInfo = read<__int64>(AudioHandle + 0x1E0);
		FQuat ViewRotation = read<FQuat>(ViewInfo);

		FRotator Buffer;
		QuatToRotator(&ViewRotation, &Buffer);

		double Yaw = Buffer.Yaw;
		double Pitch = (asin(read<double>(v2 + 0x8d0)) * (180.0 / 3.14159265358979323846264338327950288419716939937510));
		Vector3 CameraLocation = read<Vector3>(v2 + 0x4b0);
		float FOV = read<float>(LocalPlayer + 0x4CC);
		Camera LocalCamera;
		LocalCamera.Rotation = { Pitch, Yaw, 0.f};
		LocalCamera.Location = CameraLocation;
		LocalCamera.FieldOfView = FOV;
		return LocalCamera;
	}

	bool ProjectWorldToScreen(Vector3 WorldLocation, Camera vCamera, Vector3* Out)
	{
		D3DMATRIX tempMatrix = Matrix(vCamera.Rotation);
		Vector3 vAxisX = Vector3(tempMatrix.m[0][0], tempMatrix.m[0][1], tempMatrix.m[0][2]);
		Vector3 vAxisY = Vector3(tempMatrix.m[1][0], tempMatrix.m[1][1], tempMatrix.m[1][2]);
		Vector3 vAxisZ = Vector3(tempMatrix.m[2][0], tempMatrix.m[2][1], tempMatrix.m[2][2]);

		Vector3 vDelta = WorldLocation - vCamera.Location;
		Vector3 vTransformed = Vector3(vDelta.DotProduct(vAxisY), vDelta.DotProduct(vAxisZ), vDelta.DotProduct(vAxisX));
		if (vTransformed.z < 1.f)
			vTransformed.z = 1.f;

		Vector3 Screenlocation = Vector3(0, 0, 0);
		double ScreenCenterX = OverlayWidth / 2.0f;
		double ScreenCenterY = OverlayHeight / 2.0f;
		Screenlocation.x = ScreenCenterX + vTransformed.x * ((ScreenCenterX / tanf(vCamera.FieldOfView * (float)M_PI / 360.f))) / vTransformed.z;
		Screenlocation.y = ScreenCenterY - vTransformed.y * ((ScreenCenterX / tanf(vCamera.FieldOfView * (float)M_PI / 360.f))) / vTransformed.z;

		if (Screenlocation.x < 1 || Screenlocation.x > OverlayWidth || Screenlocation.y < 1 || Screenlocation.y > OverlayHeight)
			return false;

		*Out = Screenlocation;
		return true;
	}
}
