#pragma once
#include <Windows.h>

inline bool ShowMenu = true;
inline HWND GameWindow = NULL;
inline HWND Window = NULL;
inline int OverlayWidth = 0;
inline int OverlayHeight = 0;
inline void* VerdanaFont;
inline uintptr_t oBaseAddress = 0;

namespace Offsets
{
    inline DWORD64 UWorld = 0x11791DF8;
    inline DWORD OwningGameInstance = 0x1D0;
    inline DWORD RootComponent = 0x198;
    inline DWORD PlayerState = 0x2B0;
    inline DWORD Mesh = 0x318;
    inline DWORD LocalPlayers = 0x38;
    inline DWORD PlayerController = 0x30;
    inline DWORD AcknowledgedPawn = 0x338;
    inline DWORD ActorID = 0x18;
    inline DWORD PersistentLevel = 0x30;
    inline DWORD Levels = 0x170;

    inline DWORD ActorsArray = 0xA0;
    inline DWORD ActorCount = ActorsArray + 0x8;

    inline DWORD ComponentToWorld = 0x1C0;
    inline DWORD RelativeLocation = 0x120;
    inline DWORD RelativeRotation = 0x138;

    inline DWORD GameState = 0x158;
    inline DWORD PlayerArray = 0x2A8;
    inline DWORD PawnPrivate = 0x308;
}


