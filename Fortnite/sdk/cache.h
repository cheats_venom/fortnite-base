#pragma once
#include <Windows.h>
#include <vector>
#include <array>
#include <mutex>
#include "../Include/Driver.h"
#include "../sdk/structs.h"
#include "../sdk/sdk.h"
#include "structs.h"
#include "imgui.h"
#include "../Include/utils.h"
#include "../Include/Variadicstring.h"
#include "../GUI/Settings.h"

struct CachedPlayers {
	uintptr_t Pawn;
	uintptr_t RootComponent;
	uintptr_t PlayerState;
	uintptr_t Mesh;
};
std::vector<CachedPlayers> CachedPlayerList;
uintptr_t Uworld = 0;

void callCache() {
	std::vector <CachedPlayers> list;
	Uworld = SDK::read<uintptr_t>(oBaseAddress + Offsets::UWorld);
	if (!Uworld) { CachedPlayerList.clear(); return; }

	auto GameState = SDK::read<uintptr_t>(Uworld + Offsets::GameState);
	if (!GameState) { CachedPlayerList.clear(); return; }
		
	auto PlayerArray = SDK::read<uintptr_t>(GameState + Offsets::PlayerArray);
	if (!PlayerArray) { CachedPlayerList.clear(); return; }

	int PlayerNum = SDK::read<int>(GameState + Offsets::PlayerArray + 0x8);
	
	for (int i = 0; i < PlayerNum; i++) {

		auto PlayerState = SDK::read<uintptr_t>(PlayerArray + i * 0x8);
		if (!PlayerState) continue; 

		auto PawnPrivate = SDK::read<uintptr_t>(PlayerState + Offsets::PawnPrivate);
		if (!PawnPrivate) continue;

		auto Mesh = SDK::read<uintptr_t>(uintptr_t(PawnPrivate) + Offsets::Mesh);
		if (!Mesh) continue;

		auto PlayerRoot = SDK::read<uintptr_t>(uintptr_t(PawnPrivate) + Offsets::RootComponent);
		if (!PlayerRoot) continue;

		CachedPlayers cache;
		cache.Pawn = PawnPrivate;
		cache.PlayerState = PlayerState;
		cache.Mesh = Mesh;
		cache.RootComponent = PlayerRoot;
		list.push_back(cache);
	}

	CachedPlayerList = list;	
}

bool isReady = false;
void PlayerCache()
{
	while (1) {
		if (isReady) {
			callCache();
		}
		Sleep(1000);
	}	
}

bool KeyDown(int key) {
	if (key >= ImGuiKey_GamepadStart && key <= ImGuiKey_GamepadR3) {
		return ImGui::IsKeyDown((ImGuiKey)key);
	}
	else {
		return GetAsyncKeyState(key) & 0x8000;
	}
}

void DrawEntityList() {

	if (!isReady)
		isReady = true;

	if (!Uworld) { CachedPlayerList.clear(); return; }

	auto OwningGameInstance = SDK::read<uintptr_t>(Uworld + Offsets::OwningGameInstance);
	if (!OwningGameInstance) { CachedPlayerList.clear(); return; }

	auto Localplayers = SDK::read<uintptr_t>(OwningGameInstance + Offsets::LocalPlayers);
	if (!Localplayers) { CachedPlayerList.clear(); return; }

	auto LocalPlayer = SDK::read<uintptr_t>(Localplayers);
	if (!LocalPlayer) { CachedPlayerList.clear(); return; }

	auto PlayerController = SDK::read<uintptr_t>(LocalPlayer + Offsets::PlayerController);
	if (!PlayerController) { CachedPlayerList.clear(); return; }

	auto AcknowledgedPawn = SDK::read<uintptr_t>(PlayerController + Offsets::AcknowledgedPawn);
	if (!AcknowledgedPawn) { CachedPlayerList.clear(); return; }

	auto LocalRootComponent = SDK::read<uintptr_t>(uintptr_t(AcknowledgedPawn) + Offsets::RootComponent);
	if (!LocalRootComponent) { CachedPlayerList.clear(); return; }

	auto Camera = SDK::GetCamera(LocalPlayer, LocalRootComponent, Uworld, PlayerController, AcknowledgedPawn);
	auto MyPos = Camera.Location;

	for (auto cache : CachedPlayerList)
	{
		auto CurrentActor = cache.Pawn;
		auto CurrentMesh = cache.Mesh;
		auto CurrentPlayerRoot = cache.RootComponent;
		auto CurrentPlayerState = cache.PlayerState;
		if (CurrentActor)
		{
			if (AcknowledgedPawn == CurrentActor) continue;
			if (!CurrentMesh) continue;
			if (!CurrentPlayerRoot) continue;
			if (!CurrentPlayerState) continue;
				
			Vector3 RootPos = SDK::read<Vector3>(CurrentPlayerRoot + Offsets::RelativeLocation);
			Vector3 RootScreen;
			if (SDK::ProjectWorldToScreen(RootPos, Camera, &RootScreen)) {
				if (Settings.Esp.Enable) {
					ImGui::GetWindowDrawList()->AddText((ImFont*)VerdanaFont, 20.f, ImVec2((float)RootScreen.x, (float)RootScreen.y), IM_COL32_WHITE, "Player");
				}
			}
		}
	}
}

float Smooth = 2.f;
void AimPos(float TargetX, float TargetY, float ScreenCenterX, float ScreenCenterY)
{
	float TargetX_ = 0;
	float TargetY_ = 0;
	if (TargetX != 0)
	{
		if (TargetX > ScreenCenterX)
		{
			TargetX_ = -(ScreenCenterX - TargetX);
			TargetX_ /= Smooth;
			if (TargetX_ + ScreenCenterX > ScreenCenterX * 2) TargetX_ = 0;
		}

		if (TargetX < ScreenCenterX)
		{
			TargetX_ = TargetX - ScreenCenterX;
			TargetX_ /= Smooth;
			if (TargetX_ + ScreenCenterX < 0) TargetX_ = 0;
		}
	}

	if (TargetY != 0)
	{
		if (TargetY > ScreenCenterY)
		{
			TargetY_ = -(ScreenCenterY - TargetY);
			TargetY_ /= Smooth;
			if (TargetY_ + ScreenCenterY > ScreenCenterY * 2) TargetY_ = 0;
		}
		if (TargetY < ScreenCenterY)
		{
			TargetY_ = TargetY - ScreenCenterY;
			TargetY_ /= Smooth;
			if (TargetY_ + ScreenCenterY < 0) TargetY_ = 0;
		}
	}

	Driver->MoveMouse((ULONG)TargetX, (ULONG)TargetY, 0);
}

/* Usage:
	Call AimPos(HeadScreen.x, HeadScreen.y, (GameWidth / 2), (GameHeight / 2));
*/

DWORD astime_ = 0;
DWORD astime_Delay = 0;
bool IsPressed = false;

void PressLeftClick() {
	if (!IsPressed)
	{
		IsPressed = true;
		Driver->MoveMouse(0, 0, MOUSE_LEFT_BUTTON_DOWN);
	}
}

void ResetLeftClick() {
	if (IsPressed)
	{
		auto curTime = timeGetTime();
		if (curTime - astime_ >= 50)
		{
			IsPressed = false;
			Driver->MoveMouse(0, 0, MOUSE_LEFT_BUTTON_UP);
			astime_ = curTime;
		}
	}
}

/* Usage:
	Call PressLeftClick() when you want to click
	Call ResetLeftClick() on your loop to reset the click
*/