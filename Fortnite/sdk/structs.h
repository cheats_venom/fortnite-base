#pragma once
#include <Windows.h>
#include <cstdint>
#include <string>
#include <locale>
#include <d3d9.h>
using namespace std;

#define M_PI 3.14159265358979323846
#define M_PI_F ((float)(M_PI))
#define M_RADPI 57.295779513082f
#define RAD2DEG(x) ((float)(x) * (float)(180.f / M_PI_F))
#define DEG2RAD( x ) ( (float)(x) * (float)(M_PI / 180.f) )

class Vector3
{
public:
	double x, y, z;
	Vector3() :x(0.0f), y(0.0f), z(0.0f) {}
	Vector3(double _x, double _y, double _z) :x(_x), y(_y), z(_z) {}
	//Vector(const FVector& v) :x(v.X), y(v.Y), z(v.Z) {}

	double InnerProduct(const Vector3& v) const {
		return (x * v.x) + (y * v.y) + (z * v.z);
	}

	Vector3 OuterProduct(const Vector3& v) const {
		Vector3 output;
		output.x = (y * v.z) - (z * v.y);
		output.y = (z * v.x) - (x * v.z);
		output.z = (x * v.y) - (y * v.x);
		return output;
	}

	Vector3 Min(const Vector3& v) const {
		Vector3 output;
		output.x = x < v.x ? x : v.x;
		output.y = y < v.y ? y : v.y;
		output.z = z < v.z ? z : v.z;
		return output;
	}

	Vector3 Max(const Vector3& v) const {
		Vector3 output;
		output.x = x > v.x ? x : v.x;
		output.y = y > v.y ? y : v.y;
		output.z = z > v.z ? z : v.z;
		return output;
	}

	bool operator == (const Vector3& v) const {
		return x == v.x && y == v.y && z == v.z;
	}

	bool operator != (const Vector3& v) const {
		return !(*this == v);
	}

	Vector3 operator - () const {
		return Vector3(-x, -y, -z);
	}

	Vector3 operator + (const Vector3& v) const {
		return Vector3(x + v.x, y + v.y, z + v.z);
	}

	Vector3 operator - (const Vector3& v) const {
		return Vector3(x - v.x, y - v.y, z - v.z);
	}

	Vector3 operator * (const double fValue) const {
		return Vector3(x * fValue, y * fValue, z * fValue);
	}

	Vector3 operator/(float v) const
	{
		return Vector3(x / v, y / v, z / v);
	}

	inline double DotProduct(Vector3 v) const
	{
		return (x * v.x + y * v.y + z * v.z);
	}

	void Normalize() {
		double factor = 1.0f / sqrtf(x * x + y * y + z * z);
		(*this) = (*this) * factor;
	}

	double Length() const {
		return sqrtf(x * x + y * y + z * z);
	}

	double Distance(const Vector3& v) const {
		return (v - *this).Length();
	}

	Vector3 operator ^ (const Vector3& v) const {
		return OuterProduct(v);
	}

	double operator * (const Vector3& v) const {
		return InnerProduct(v);
	}
};

struct FRotator
{
	double Pitch;
	double Yaw;
	double Roll;

	FRotator()
	{
	}
	FRotator(double flPitch, double flYaw, double flRoll)
	{
		Pitch = flPitch;
		Yaw = flYaw;
		Roll = flRoll;
	}

	double Length()
	{
		return sqrt(Pitch * Pitch + Yaw * Yaw + Roll * Roll);
	}

	FRotator Clamp()
	{
		if (Pitch > 180)
			Pitch -= 360;
		else if (Pitch < -180)
			Pitch += 360;
		if (Yaw > 180)
			Yaw -= 360;
		else if (Yaw < -180)
			Yaw += 360;
		if (Pitch < -89)
			Pitch = -89;
		if (Pitch > 89)
			Pitch = 89;
		while (Yaw < -180.0f)
			Yaw += 360.0f;
		while (Yaw > 180.0f)
			Yaw -= 360.0f;
		Roll = 0;
		return *this;
	}

	FRotator operator-(FRotator angB)
	{

		return FRotator(Pitch - angB.Pitch, Yaw - angB.Yaw, Roll - angB.Roll);
	}

	FRotator operator+(FRotator angB)
	{

		return FRotator(Pitch + angB.Pitch, Yaw + angB.Yaw, Roll + angB.Roll);
	}
};

struct FQuat
{
	double x;
	double y;
	double z;
	double w;
};

inline D3DMATRIX Matrix(Vector3 rot, Vector3 origin = Vector3(0, 0, 0))
{
	double radPitch = (rot.x * double(M_PI) / 180.f);
	double radYaw = (rot.y * double(M_PI) / 180.f);
	double radRoll = (rot.z * double(M_PI) / 180.f);

	double SP = sinf(radPitch);
	double CP = cosf(radPitch);
	double SY = sinf(radYaw);
	double CY = cosf(radYaw);
	double SR = sinf(radRoll);
	double CR = cosf(radRoll);

	D3DMATRIX matrix;
	matrix.m[0][0] = CP * CY;
	matrix.m[0][1] = CP * SY;
	matrix.m[0][2] = SP;
	matrix.m[0][3] = 0.f;

	matrix.m[1][0] = SR * SP * CY - CR * SY;
	matrix.m[1][1] = SR * SP * SY + CR * CY;
	matrix.m[1][2] = -SR * CP;
	matrix.m[1][3] = 0.f;

	matrix.m[2][0] = -(CR * SP * CY + SR * SY);
	matrix.m[2][1] = CY * SR - CR * SP * SY;
	matrix.m[2][2] = CR * CP;
	matrix.m[2][3] = 0.f;

	matrix.m[3][0] = origin.x;
	matrix.m[3][1] = origin.y;
	matrix.m[3][2] = origin.z;
	matrix.m[3][3] = 1.f;

	return matrix;
}

FRotator* QuatToRotator(FQuat* Current, FRotator* retstr)
{
	float Z; // xmm4_4
	float Y; // xmm0_4
	float W; // xmm2_4
	float v7; // xmm5_4
	float v8; // xmm7_4
	float v9; // xmm8_4
	float v10; // xmm0_4
	float v11; // xmm1_4
	float v12; // xmm7_4
	float X; // xmm0_4
	float v14; // xmm0_4
	float v15; // xmm0_4
	float v16; // xmm0_4
	float v17; // xmm1_4
	float v18; // xmm5_4
	float v19; // xmm2_4
	float v20; // xmm4_4
	float v21; // xmm0_4
	float v22; // xmm0_4
	float v23; // xmm2_4
	float v24; // xmm4_4

	Z = Current->z;
	Y = Current->y;
	W = Current->w;
	v7 = (float)(Current->x * Z) - (float)(Y * W);
	v8 = (float)((float)(Y * Current->x) + (float)(Y * Current->x)) + (float)((float)(W * Z) + (float)(W * Z));
	v9 = 1.0 - (float)((float)((float)(Y * Y) + (float)(Y * Y)) + (float)((float)(Z * Z) + (float)(Z * Z)));
	if (v7 >= -0.49999949)
	{
		if (v7 <= 0.49999949)
		{
			v18 = v7 + v7;
			v19 = v18;
			v20 = sqrtf(fmaxf(1.0 - v19, 0.0))
				* (float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)(0.0066700899 - (float)(v19 * 0.0012624911)) * v19)
					- 0.017088126)
					* v19)
					+ 0.03089188)
					* v19)
					- 0.050174303)
					* v19)
					+ 0.088978991)
					* v19)
					- 0.2145988)
					* v19)
					+ 1.5707963);
			if (v18 < 0.0)
				v21 = v20 - 1.5707963;
			else
				v21 = 1.5707963 - v20;
			retstr->Pitch = v21 * 57.295776;
			v22 = atan2(v8, v9);
			v23 = Current->x * Current->x;
			v24 = Current->x * Current->w;
			retstr->Yaw = v22 * 57.295776;
			v15 = atan2(
				(float)((float)(Current->y * Current->z) * -2.0) - (float)(v24 + v24),
				1.0 - (float)((float)(v23 + v23) + (float)((float)(Current->y * Current->y) + (float)(Current->y * Current->y))))
				* 57.295776;
			goto LABEL_13;
		}
		retstr->Pitch = 90.0;
		v16 = atan2(v8, v9);
		v17 = Current->w;
		retstr->Yaw = v16 * 57.295776;
		v14 = retstr->Yaw - (float)((float)(atan2(Current->x, v17) * 2.0) * 57.295776);
	}
	else
	{
		retstr->Pitch = -90.0;
		v10 = atan2(v8, v9);
		v11 = Current->w;
		v12 = v10 * 57.295776;
		X = Current->x;
		retstr->Yaw = v12;
	}
LABEL_13:
	retstr->Roll = 0;
	return retstr;
}