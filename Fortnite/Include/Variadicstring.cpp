
#include <cstdarg>
#include "Variadicstring.h"
#include <memory>
#include <list>
std::string va_string(std::string Format, ...)
{
	auto Resultbuffer = std::make_unique<char[]>(2049);
	std::va_list Varlist;

	//Create a new string from the arguments and truncate as needed.
	va_start(Varlist, Format);
	std::vsnprintf(Resultbuffer.get(), 2048, Format.c_str(), Varlist);
	va_end(Varlist);

	return std::string(Resultbuffer.get());
}


std::wstring va_wstring(std::wstring Format, ...)
{
	auto Resultbuffer = std::make_unique<wchar_t[]>(2049);
	std::va_list Varlist;

	//Create a new string from the arguments and truncate as needed.
	va_start(Varlist, Format);
	std::vswprintf(Resultbuffer.get(), 2048, Format.c_str(), Varlist);
	va_end(Varlist);

	return std::wstring(Resultbuffer.get());
}

