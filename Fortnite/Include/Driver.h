#pragma once
#include <iostream>
#include <Windows.h>

#define NT_SUCCESS(Status)               (((NTSTATUS)(Status)) >= 0)
#define STATUS_UNSUCCESSFUL              ((NTSTATUS)0xC0000001L)
#define STATUS_SUCCESS                   ((NTSTATUS)0x00000000L)

#define MOUSE_LEFT_BUTTON_DOWN   0x0001
#define MOUSE_LEFT_BUTTON_UP     0x0002
#define MOUSE_RIGHT_BUTTON_DOWN  0x0004
#define MOUSE_RIGHT_BUTTON_UP    0x0008
#define MOUSE_MIDDLE_BUTTON_DOWN 0x0010
#define MOUSE_MIDDLE_BUTTON_UP   0x0020

namespace Addon {
	NTSTATUS Init();
	DWORD AttachProcess(const char* processName);
	DWORD AttachProcessPID(DWORD pid);
	void* GetProcessBase();
	void* GetProcessPeb();
	void* GetProcessModule(const char* ImageName);
	NTSTATUS ReadProcessMem(uintptr_t Address, PVOID Buffer, SIZE_T Size);
	void WriteProcessMem(uintptr_t Address, PVOID Value, SIZE_T Size);
	NTSTATUS HideCreatedWindow(PVOID Window, UINT32 FlagValue); //hide from screenshot
	NTSTATUS MaskCreatedWindow(PVOID Window); //hide window to anticheat (P.S: Create a window that takes up the entire screen so you don't need to move or change it once it's created. Otherwise it will be exposed until you call MaskCreatedWindow again.)
	NTSTATUS MoveMouse(ULONG x, ULONG y, USHORT flag);
	NTSTATUS _AllocMemory(ULONG ProcessPid, ULONG Size, PVOID* OutBaseAddress);
	void* GetPml4Base();
	NTSTATUS ReadValorantShadowRegion(uintptr_t Address, PVOID Buffer, SIZE_T Size);
	void WriteValorantShadowRegion(uintptr_t Address, PVOID Value, SIZE_T Size);

	typedef struct _lpReserved
	{
		decltype(&Init)(Init);
		decltype(&AttachProcess)(AttachProcess);
		decltype(&AttachProcessPID)(AttachProcessPID);
		decltype(&GetProcessBase)(GetProcessBase);
		decltype(&GetProcessPeb)(GetProcessPeb);
		decltype(&GetProcessModule)(GetProcessModule);
		decltype(&ReadProcessMem)(ReadProcessMem);
		decltype(&WriteProcessMem)(WriteProcessMem);
		decltype(&HideCreatedWindow)(HideCreatedWindow);
		decltype(&MaskCreatedWindow)(MaskCreatedWindow);
		decltype(&MoveMouse)(MoveMouse);
		decltype(&_AllocMemory)(_AllocMemory);
		decltype(&GetPml4Base)(GetPml4Base);
		decltype(&ReadValorantShadowRegion)(ReadValorantShadowRegion);
		decltype(&WriteValorantShadowRegion)(WriteValorantShadowRegion);
	};
}

inline Addon::_lpReserved* Driver = 0;

inline bool InitDriver(LPVOID lpReserved) {
	Driver = reinterpret_cast<Addon::_lpReserved*>(lpReserved);
	return NT_SUCCESS(Driver->Init());
}
