#pragma once
#include "windows.h"
#include <string>

extern int GetRandom(int min, int max);
extern void AimPos(float tarx, float tary, float ScreenCenterX, float ScreenCenterY, bool isAimLock);
extern PVOID ShowMessageBox(const char* msg, const char* Title, DWORD Type, int Time, bool Exit);