#include "utils.h"
#include "Driver.h"
#include "PortableExecutable/PortableExecutable.h"

int MyMessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType, DWORD dwMilliseconds)
{
        typedef int(__stdcall* MSGBOXAAPI)(IN HWND hWnd,
            IN LPCSTR lpText, IN LPCSTR lpCaption,
            IN UINT uType, IN WORD wLanguageId, IN DWORD dwMilliseconds);

    auto MMessageBox = [&](HWND hWnd, LPCSTR lpText,
        LPCSTR lpCaption, UINT uType, WORD wLanguageId,
        DWORD dwMilliseconds) -> int {
            MSGBOXAAPI MsgBoxTOA = NULL;

            if (!MsgBoxTOA)
            {
                HMODULE hUser32 = GetModuleA(("user32.dll"));
                if (hUser32)
                {
                    MsgBoxTOA = (MSGBOXAAPI)GetExportAddress(hUser32,
                        ("MessageBoxTimeoutA"), true);
                }
                else {
                    return 0;
                }
            }

            if (MsgBoxTOA)
            {
                return MsgBoxTOA(hWnd, lpText, lpCaption,
                    uType, wLanguageId, dwMilliseconds);
            }
    };

    return MMessageBox(hWnd, lpText, lpCaption, uType, 0, dwMilliseconds);
}

PVOID ShowMessageBox(const char* msg, const char* Title, DWORD Type, int Time, bool Exit)
{

    MyMessageBox(NULL, msg, Title, MB_SYSTEMMODAL | Type, Time);

    if (Exit)
        (ExitProcess)(0x100);

    return NULL;
}