/*
       Create a new string from variadic arguments.
       va("%i hedgehogs!", 42) == "42 hedgehogs!" 
*/

#pragma once


#include <string>

std::string va_string(std::string Format, ...);
std::wstring va_wstring(std::wstring Format, ...);




