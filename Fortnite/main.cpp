#include <Windows.h>
#include <dwmapi.h>
#include <iostream>
#include "imgui.h"
#include "imgui_impl_win32.h"
#include "Include/Driver.h"
#include <mutex>
#include <d3d11.h>
#include "sdk/cache.h"
#include "sdk/offsets.h"
#include "imgui_impl_dx11.h"
#include <shlobj.h>
#include  <io.h>
#include "Include/PortableExecutable/PortableExecutable.h"
#include "GUI/Gui.h"

#pragma comment (lib, "winmm.lib")
#pragma comment (lib, "dwmapi.lib")
#pragma comment (lib, "d3d11.lib")

ID3D11Device* g_pd3dDevice = NULL;
ID3D11DeviceContext* g_pd3dDeviceContext = NULL;
IDXGISwapChain* g_pSwapChain = NULL;
ID3D11RenderTargetView* g_mainRenderTargetView = NULL;

void CleanupRenderTarget()
{
	if (g_mainRenderTargetView) { g_mainRenderTargetView->Release(); g_mainRenderTargetView = NULL; }
}

void CreateRenderTarget()
{
	ID3D11Texture2D* pBackBuffer;
	g_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pBackBuffer));
	g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &g_mainRenderTargetView);
	pBackBuffer->Release();
}

void Shutdown()
{
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
	PostQuitMessage(0);
	(exit)(0x100);
}



void LoadKeys(ImGuiIO& io) {
	bool isShift = ((GetAsyncKeyState)(VK_SHIFT) & 0x8000) || ((GetAsyncKeyState)(VK_LSHIFT) & 0x8000) || ((GetAsyncKeyState)(VK_RSHIFT) & 0x8000);
	bool isCaps = (GetKeyState)(0x14);
	for (int i = '0'; i < '9'; i++) {
		if ((GetAsyncKeyState)(i) & 1) {
			io.AddInputCharacter(i);
		}
	}
	for (int i = 'A'; i < 'Z'; i++) {
		if ((GetAsyncKeyState)(i) & 1) {
			if (isCaps && !isShift) {
				io.AddInputCharacter(i);
			}
			else if (isShift && !isCaps) {
				io.AddInputCharacter(i);
			}
			else {
				io.AddInputCharacter(i + 32);
			}
		}
	}
}

void DrawCheat() {

	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

	ImGuiIO& io = ImGui::GetIO();
	
	io.MouseDown[0] = (GetAsyncKeyState)(VK_LBUTTON) & 0x8000;
	io.MouseDown[1] = (GetAsyncKeyState)(VK_RBUTTON) & 0x8000;
	io.MouseDown[2] = (GetAsyncKeyState)(VK_MBUTTON) & 0x8000;
	io.MouseDown[3] = (GetAsyncKeyState)(VK_XBUTTON1) & 0x8000;
	io.MouseDown[4] = (GetAsyncKeyState)(VK_XBUTTON2) & 0x8000;

	for (int i = 0; i < 512; i++) {
		auto key = ImGui_ImplWin32_VirtualKeyToImGuiKey(i);
		if (key != ImGuiKey_None) {
			io.AddKeyEvent(key, ((GetKeyState)(i) & 0x8000) != 0);
		}
	}
	LoadKeys(io);

	if (((GetKeyState)(VK_LEFT) & 0x8000) != 0) {
		io.MouseWheelH = +0.2;
	}
	if (((GetKeyState)(VK_RIGHT) & 0x8000) != 0) {
		io.MouseWheelH = -0.2;
	}
	if (((GetKeyState)(VK_UP) & 0x8000) != 0) {
		io.MouseWheel = +0.2;
	}
	if (((GetKeyState)(VK_DOWN) & 0x8000) != 0) {
		io.MouseWheel = -0.2;
	}

	{
		if (GetAsyncKeyState(VK_INSERT) & 1) {
			ShowMenu = !ShowMenu;
			if (!ShowMenu) {
				SettingsHelper::SaveSettings();
			}
		}
	}

	ImGui::SetNextWindowBgAlpha(0.f);
	ImGui::SetNextWindowPos({ 0, 0 }, ImGuiCond_Always);
	ImGui::SetNextWindowSize({ io.DisplaySize.x, io.DisplaySize.y }, ImGuiCond_Always);

	ImGui::Begin(("##scene"), nullptr, ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoTitleBar);
	{
		pGUi.Render(ShowMenu);
		DrawEntityList();
	}

	ImGui::End();
	ImGui::EndFrame();
	float clearColor[4] = { 0.0f,0.0f,0.0f,0.0f };
	g_pd3dDeviceContext->ClearRenderTargetView(g_mainRenderTargetView, clearColor);
	ImGui::Render();
	g_pd3dDeviceContext->OMSetRenderTargets(1, &g_mainRenderTargetView, NULL);
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
	g_pSwapChain->Present(0, 0);


}

LRESULT CALLBACK WndProcHook(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
		switch (Message)
		{
		case WM_DESTROY:
			Shutdown();
			break;
		case WM_SIZE:
			if (g_pd3dDevice != NULL && wParam != SIZE_MINIMIZED)
			{
				CleanupRenderTarget();
				g_pSwapChain->ResizeBuffers(0, (UINT)LOWORD(lParam), (UINT)HIWORD(lParam), DXGI_FORMAT_UNKNOWN, 0);
				CreateRenderTarget();
			}
			break;
		}
	return ::DefWindowProc(hWnd, Message, wParam, lParam);
}

void SecurityChecks() {
	if ((FindWindowA)(("YourWindowName"), ("YourWindowName")))
	{
		if (!NT_SUCCESS(Driver->MaskCreatedWindow(Window))) {
			ShowMessageBox(("Error #0004"), ("Error"), MB_SYSTEMMODAL | MB_ICONERROR, 3000, true);
			return;
		}
	}
}

void Loop()
{
	RECT old_rc = { 0,0,0,0 };
	MSG Message = { NULL };
	ZeroMemory(&Message, sizeof(MSG));

	while (Message.message != WM_QUIT)
	{
		if (PeekMessage(&Message, Window, 0, 0, PM_REMOVE))
		{
			(TranslateMessage)(&Message);
			(DispatchMessage)(&Message);
		}

		GameWindow = (FindWindowA)(("UnrealWindow"), ("Fortnite  "));
		if ((GetWindow)(GameWindow, (UINT)NULL) == NULL) {
			SendMessage(Window, WM_DESTROY, 0, 0);
			break;
		}

		SecurityChecks();

		HWND hwnd_active = (GetForegroundWindow)();
		if (hwnd_active == GameWindow || hwnd_active == Window) {
			RECT rc;
			POINT xy;

			ZeroMemory(&rc, sizeof(RECT));
			ZeroMemory(&xy, sizeof(POINT));
			(GetClientRect)(GameWindow, (LPRECT)&rc);
			(ClientToScreen)(GameWindow, (LPPOINT)&xy);
			rc.left = xy.x;
			rc.top = xy.y;

			if (rc.left != old_rc.left || rc.right != old_rc.right || rc.top != old_rc.top || rc.bottom != old_rc.bottom)
			{
				old_rc = rc;
				OverlayWidth = rc.right;
				OverlayHeight = rc.bottom;
				SetWindowPos(Window, (HWND)0, xy.x, xy.y, OverlayWidth, OverlayHeight, SWP_NOREDRAW);
			}

			ImGui::GetIO().MouseDrawCursor = ShowMenu;

			DrawCheat();

			HWND hwnd = GetWindow(hwnd_active, (UINT)GW_HWNDPREV);
			if (hwnd_active == Window)
			{
				hwnd = GetWindow(GameWindow, (UINT)GW_HWNDPREV);
			}
			SetWindowPos(Window, hwnd, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

		}
		else {
			float clearColor[4] = { 0.0f,0.0f,0.0f,0.0f };
			g_pd3dDeviceContext->ClearRenderTargetView(g_mainRenderTargetView, clearColor);
			g_pd3dDeviceContext->OMSetRenderTargets(1, &g_mainRenderTargetView, NULL);
			g_pSwapChain->Present(0, 0); // Present without vsync;
		}
		(Sleep)(1);
	}


}

void GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);
	horizontal = desktop.right;
	vertical = desktop.bottom;
}

bool CreateOWindow()
{
	const char* ClassName = ("YourWindowName");

	WNDCLASSEX wc;
	ZeroMemory(&wc, sizeof(wc));
	wc.cbSize = sizeof(wc);
	wc.lpfnWndProc = WndProcHook;
	wc.lpszClassName = ClassName;
	(RegisterClassExA)(&wc);

	Window = (CreateWindowExA)(
		(DWORD)NULL,
		ClassName,
		ClassName,
		WS_POPUP | WS_VISIBLE,
		0, 0, 0, 0,
		(HWND)0, (HMENU)0, (HINSTANCE)0, (LPVOID)0);

	if (!Window)
		return FALSE;

	(SetWindowLongA)(Window, GWL_EXSTYLE, WS_EX_TOOLWINDOW | WS_EX_LAYERED | WS_EX_TRANSPARENT);

	MARGINS Margin = { -1 };
	DwmExtendFrameIntoClientArea(Window, &Margin);
	(ShowWindow)(Window, SW_SHOW);
	(UpdateWindow)(Window);

	if (!Window)
		return FALSE;

	return TRUE;
}

bool CreateDeviceD3D()
{
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 2;
	sd.BufferDesc.Width = 0;
	sd.BufferDesc.Height = 0;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = Window;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	UINT createDeviceFlags = 0;
	D3D_FEATURE_LEVEL featureLevel;
	const D3D_FEATURE_LEVEL featureLevelArray[2] = { D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_0, };

	auto addr = GetModuleA(("d3d11.dll"));
	if (!addr)
		addr = (LoadLibraryA)((("d3d11.dll")));

	auto D3D11CreateDeviceAndSwapChain_ = GetExportAddress(addr, ("D3D11CreateDeviceAndSwapChain"), true);
	if (!D3D11CreateDeviceAndSwapChain_)
		return false;

	if (reinterpret_cast<decltype(&D3D11CreateDeviceAndSwapChain)>(D3D11CreateDeviceAndSwapChain_)(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, featureLevelArray, 2, D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, &featureLevel, &g_pd3dDeviceContext) != S_OK)
		return false;

	ID3D11Texture2D* pBackBuffer;
	g_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pBackBuffer));
	g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &g_mainRenderTargetView);
	pBackBuffer->Release();

	return true;
}

bool InitializeD3D()
{
	if (!CreateDeviceD3D())
	{
		return false;
	}

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	(void)io;
	io.IniFilename = nullptr;
	io.LogFilename = nullptr;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

	VerdanaFont = io.Fonts->AddFontFromFileTTF(("C:\\Windows\\Fonts\\Verdana.ttf"), 20.0f);

	pGUi.Initialize(g_pd3dDevice);

	auto ret = ImGui_ImplWin32_Init(Window);
	if (!ret)
		return false;

	ret = ImGui_ImplDX11_Init(g_pd3dDevice, g_pd3dDeviceContext);
	if (!ret)
		return false;

	return true;
}

void EntryPoint() {

	auto ret = CreateOWindow();
	if (!ret) {
		ShowMessageBox(("Error #0002"), ("Error"), MB_SYSTEMMODAL | MB_ICONERROR, 3000, true);
		return;
	}


	auto Status = Driver->HideCreatedWindow(Window, WDA_EXCLUDEFROMCAPTURE);
	if (!NT_SUCCESS(Status)) {
		ShowMessageBox(("Error #0003"), ("Error"), MB_SYSTEMMODAL | MB_ICONERROR, 3000, true);
		return;
	}

	Status = Driver->MaskCreatedWindow(Window);
	if (!NT_SUCCESS(Status)) {
		ShowMessageBox(("Error #0004"), ("Error"), MB_SYSTEMMODAL | MB_ICONERROR, 3000, true);
		return;
	}

	{
		SettingsHelper::Initialize();
	}

	ret = InitializeD3D();
	if (!ret) {

		ShowMessageBox(("Error #0005"), ("Error"), MB_SYSTEMMODAL | MB_ICONERROR, 3000, true);
		return;
	}

	do {
		GameWindow = (FindWindowA)((LPCSTR)0, ("Fortnite  "));
		if (GameWindow)
			break;
		(Sleep)(100);
	} while (TRUE);

	DWORD processID = 0;
	auto info = GetWindowThreadProcessId(GameWindow, &processID);
	if (!processID) {
		ShowMessageBox(("Error #0006"), ("Error"), MB_SYSTEMMODAL | MB_ICONERROR, 3000, true);
		return;
	}

	Driver->AttachProcessPID(processID);

	while (!oBaseAddress || oBaseAddress == (uint64_t)-1) {
		oBaseAddress = uint64_t(Driver->GetProcessBase());
		printf("Waiting BaseAddress %llx\n", oBaseAddress);
		(Sleep)(1000);
	}

	printf("BaseAddress %llx\n", oBaseAddress);

	isReady = true;

	Loop();
	Shutdown();


}

BOOL WINAPI DllMain(
	HINSTANCE hModule,
	DWORD fdwReason,
	LPVOID lpReserved)
{
		switch (fdwReason)
		{
		case DLL_PROCESS_ATTACH:
			{ //Test console
				AllocConsole();
				FILE* file;
				freopen_s(&file, "CONOUT$", "w", stdout);
				printf("Injected\n");
			}
			

			(DisableThreadLibraryCalls)(hModule);
			if (!InitDriver(lpReserved)) {
				ShowMessageBox(("Error #0001"), ("Error"), MB_SYSTEMMODAL | MB_ICONERROR, 3000, true);
			}

			std::thread(EntryPoint).detach();
			std::thread(PlayerCache).detach();
			ShowMessageBox(("Injected"), ("Info"), MB_SYSTEMMODAL | MB_OK | MB_ICONINFORMATION, 3000, false);

			break;
		}

	return TRUE;


}