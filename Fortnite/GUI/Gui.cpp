﻿#define  IMGUI_DEFINE_MATH_OPERATORS
#include "imgui.h"
#include "imgui_internal.h"
#include "../sdk/offsets.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "Gui.h"
#include "Settings.h"

CGui pGUi = CGui();

void CGui::Initialize(ID3D11Device* d3dDevice) {

    ImGuiIO& io = ImGui::GetIO(); (void)io;

    ImGui::StyleColorsDark();

    ImGuiStyle* style = &ImGui::GetStyle();
    ImVec4* colors = style->Colors;

    colors[ImGuiCol_FrameBg] = ImVec4(0.34f, 0.34f, 0.34f, 0.54f);
    colors[ImGuiCol_FrameBgHovered] = ImVec4(0.34f, 0.34f, 0.34f, 0.64f);
    colors[ImGuiCol_FrameBgActive] = ImVec4(0.34f, 0.34f, 0.34f, 0.74f);
    colors[ImGuiCol_Button] = ImVec4(0.34f, 0.34f, 0.34f, 0.54f);
    colors[ImGuiCol_ButtonHovered] = ImVec4(0.34f, 0.34f, 0.34f, 0.64f);
    colors[ImGuiCol_ButtonActive] = ImVec4(0.34f, 0.34f, 0.34f, 0.74f);

    //Load Fonts
    //Initialize textures
}

bool init = true;
void CGui::Render(bool &ShowMenu)
{

    if (!ShowMenu)
        return;

    if (init) {
        ImGui::SetNextWindowPos({ 300, 300 });
        init = false;
    }

    ImGui::SetNextWindowSize(ImVec2(300, 300));
    ImGui::Begin("Fortnite", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoBringToFrontOnFocus);
    {
        const auto& p = ImGui::GetWindowPos();
        ImGui::BeginChild("Child0", ImVec2(300, 300), true);
        {
            //draw functions
            ImGui::SetCursorPos(ImVec2(15, 15));
            ImGui::BeginGroup();
            {
                ImGui::Checkbox("Enable ESP", &Settings.Esp.Enable);
            }
            ImGui::EndGroup();
        }
        ImGui::EndChild();
    } ImGui::End();

}
