#include "settings.h"
#include <string>

SETTINGS Settings = { 0 };
FILE* file;

namespace SettingsHelper {
	
	void SaveSettings() {
		fopen_s(&file, "C:\\Fortnite.Settings", ("wb"));
		if (file) {
			fwrite(&Settings, sizeof(Settings), (size_t)1, file);
			fclose(file);
		}
	}

	void ResetSettings() {

		Settings = SETTINGS();

		SaveSettings();
	}

	void Initialize() {
		fopen_s(&file, "C:\\Fortnite.Settings", ("rb"));
		if (file) {
			fseek(file, (long)0, (int)SEEK_END);
			auto size = ftell(file);
			if (size == sizeof(Settings)) {
				fseek(file, (long)0, (int)SEEK_SET);
				fread((void*)&Settings, sizeof(Settings), (size_t)1, file);
				fclose(file);
			}
			else {
				fclose(file);
				ResetSettings();
			}
		}
		else {
			ResetSettings();
		}
	}
}