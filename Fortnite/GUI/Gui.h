#pragma once
#include <Windows.h>
#include <vector>
#include <d3d11.h>
#include <string>

class CGui
{
public:
    void Initialize(ID3D11Device* d3dDevice);
    void Render(bool& ShowMenu);
};

extern CGui pGUi;