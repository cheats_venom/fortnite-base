#pragma once
#include <direct.h>
#include <stdio.h>
#include <Windows.h>
#include <psapi.h>
#include <intrin.h>
#include <vector>

struct SETTINGS {
    struct ESPStruct{
        bool Enable = true;
    }Esp;
};

extern SETTINGS Settings;
namespace SettingsHelper {
    void SaveSettings();
    void ResetSettings();
    void Initialize();
}
